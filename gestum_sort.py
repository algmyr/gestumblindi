# vim: fdm=marker:
#
# Fourth version. A new version of the same thinking, spun a bit differently.
#

# Method specific functions {{{1
## Indexify {{{2
#
# A variant of the indexing in the original. This time everything is sorted,
# which will shrink the size of the index (more overlaps) and maxe for a faster
# traversal later.  The downside is that indexing is now slower due to the sorting.
#
# Time complexity:
#     O(W*L^2*log(L)),  W is # of words, L is average length of words
# 
def indexify(words):
    index = {}
    for i,word in enumerate(words):
        # Make disposable reference
        ix = index
        
        for letter in sorted(word):
            # Create dictionary if necessary
            if letter not in ix:
                ix[letter] = {}
            ix = ix[letter]
        ix[i] = i
    return index

## Init function for word search {{{2
def wordsearch(s, index, N):
    wordlist = []
    
    if N == None:
        N = len(s)
        ws_rec(wordlist, sorted(list(s)), index, N)
    else:
        ws_recN(wordlist, sorted(list(s)), index, N)
    
    return wordlist

## Recursive word search {{{2
#
# Changes:
#   Sort the phrase and generate only the words in sorted order.
#   This reduces the search space immensely.
#

# Words of length <=N
def ws_rec(wordlist, phrase, index, N):
    wordlist.extend(key  for key in index if type(key) == int);
    
    if N == 0:
        return

    P = sorted(list(set(phrase)))
    for letter in P:
        if letter in index:
            ws_rec(wordlist, phrase[phrase.index(letter)+1:], index[letter], N-1)

# Words of length N
def ws_recN(wordlist, phrase, index, N):
    if N == 0:
        wordlist.extend(key  for key in index if type(key) == int);
        return

    P = sorted(list(set(phrase)))
    for letter in P:
        if letter in index:
            ws_recN(wordlist, phrase[phrase.index(letter)+1:], index[letter], N-1)

# Functions for modularity {{{1
## Indexify file (for modularity) {{{2
def index_file(path):
    with open(path) as f:
        words = f.read().splitlines()
        index = indexify(words)
    return index,words
## Find words {{{
def find_words(phrase, index, words=[], N=None):
    found_indexes = wordsearch(phrase, index, N)
    found_words = [words[i] for i in sorted(found_indexes)]
    return found_words

