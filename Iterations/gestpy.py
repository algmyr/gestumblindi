# vim: fdm=marker:
#
# First version, brute-force like approach where
# all N-char or lower strings of the phrase is 
# generated. The dictionary is still indexed though.
#
# {{{1
def posstree(s, N=-1):
    if N == -1:
        N = len(s)
    D = {}
    rec(D,list(s),N)
    return D

def rec(D, s, N):
    if N == 0:
        return

    for i in range(0,len(s)):
        if s[i] not in D:
            D[s[i]] = {}
            rec(D[s[i]], s[:i]+s[i+1:], N-1)

def checkwalk(tree, index, word=''):
    if '\0' in index:
        print(word)
    for letter,tail in sorted(tree.items()):
        if letter in index:
            checkwalk(tail, index[letter], word+letter)

def walk(D, word=''):
    for key,val in sorted(D.items()):
        if val != {'\0':0}:
            walk(val,word+key)
        else:
            print(word+key)
# }}}1

def indexify(index, s):
    ix = index
    for c in s:
        if c not in ix:
            ix[c] = {}
        ix = ix[c]
    ix['\0'] = 0


f = open("dict")
index = {}
for line in f.readlines():
    indexify(index, line[:-1])
f.close()


msg = "optimizationmatters"
T = posstree(msg, 5)
checkwalk(T, index)

