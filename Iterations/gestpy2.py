# vim: fdm=marker:
#
# Second version, smart usage of the index improves
# the performance immensely.
#
def walk(D, word=''):
    for key,val in sorted(D.items()):
        if val != {'\0':0}:
            walk(val,word+key)
        else:
            print(word+key)

def indexify(index, s):
    ix = index
    for c in s:
        if c not in ix:
            ix[c] = {}
        ix = ix[c]
    ix['\0'] = 0


f = open("dict3")
index = {}
for line in f.readlines():
    indexify(index, line[:-1])
f.close()


msg = "optimizationmatters"

# Combine posstree and checkwalk
#T = posstree(msg, 5)
#checkwalk(T, index)


def posstree(s, index, N=-1):
    if N == -1:
        N = len(s)
    rec(list(s),index,N)

def rec(s, index, N, word=''):
    if '\0' in index:
        print(word)
    
    if N == 0:
        return

    # ta bort dubletter
    S = sorted(list(set(s)))
    for c in S:
        if c in index:
            tmp = s[:]
            tmp.remove(c)
            rec(tmp, index[c], N-1, word+c)

posstree(msg, index, 19)
