# vim: fdm=marker:
#
# Third version. Code has been cleaned and explanations
# has been added. The performance is great!
#

import time

class Timer:    
    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.interval = self.end - self.start

WORD_END = -1

###
#
# Turns a list of words into an index like
#
# these
# are
# the
# wonky
# words
#
# @->a->r->e->0
#    |
#    t->h->e->0
#    |        |
#    |        s->e->0
#    |
#    w->o->n->k->y->0
#          |
#          r->d->s->0
#
# Implemented as a dictionary of dictionaries, where the keys are letters.
#
# Here @ represent the first dictionary with keys a,t,w (first letters).
# Each key means a new dictionary and a new word branch. 
# The zeroes represent a word ending (these must not block the from view).
#
#
# This method is surprisingly fast and is a one time process anyway,
# assuming you use a static word list (and save your work).
#
# Time complexity:
#     O(W*L),  W is # of words, L is average length of words
#     (it's linear w.r.t. char count)
# 
# Space complexity:
#     Worst case: O(L^26)
#     (worst case doesn't depend on word count, only word length)
#     
#     Average case will be much lower, especially since it's words
#     we're dealing with (a lot of patterns). Worst case assumes
#     we'll encounter [a-z][a-z][a-z]..., which is absurd in reality.
#
#
def indexify(words):
    index = {}
    for word in words:
        # Make disposable reference
        ix = index
        for letter in word:
            # Create dictionary if necessary
            if letter not in ix:
                ix[letter] = {}
            ix = ix[letter]
        ix[WORD_END] = 0
    return index


# Init function for word search
def wordsearch(s, index, N=-1):
    if N == -1:
        N = len(s)

    wordlist = []
    ws_rec(wordlist, list(s), index, N)
    return wordlist

###
#
# To find matching words:
#     - Generate possible combinations of letters in the phrase
#     - Check each letter against the indexed dictionary
#     - If it's in there, move on. Otherwise abort the branch.
#     - If we see that we have a WORD_END index, we have a word!
#
# Time complexity:
#     Worst case: O(P!),  P is length of phrase
#     
#     Average case will be much better because bad patterns will
#     be found quickly and those branches aborted. The worst case
#     assumes that all combination of letters of P is a word in
#     the dictionary.
#
#     In one test: P! = 121645100408832000
#                  Actual "operations" = 165793
#
#     Tests gives avg ~ O(n^4.4), based on time taken
#
# 
# Space complexity:
#     O(W*L), W = # of matching words and L = avg word length
#     (so linear with char count)
#
#
def ws_rec(wordlist, phrase, index, N, word=''):
    if WORD_END in index:
        wordlist.append(word)
    
    if N == 0:
        return

    # Remove duplicates (and sort, optional)
    P = sorted(list(set(phrase)))
    for letter in P:
        if letter in index:
            # Make disposable copy of phrase
            tmp = phrase[:]
            tmp.remove(letter)
            ws_rec(wordlist, tmp, index[letter], N-1, word+letter)


with Timer() as t:
    # Read a dictionary and index the words
    f = open("dict3")
    words = f.read().splitlines()
    index = indexify(words)
    f.close()
print('Read and indexing took %.05f sec.' % t.interval)

S = len(str(index).split(':')) - 1
print(S)

with Timer() as t:
    # Search the words
    phrase = "optimizationmatters"
    found_words = wordsearch(phrase, index)
print('Word search took %.03f sec.' % t.interval)



# Print some stats
print('\nWord in word list: %d words' % (len(words)))
print('Average word length in word list is %.01f characters.' % (sum([len(w) for w in words])/len(words)))
print('Number of words in \"%s\": %d.' % (phrase, len(found_words)))
print('Average word length among found words: %.01f characters.\n' % (sum([len(w) for w in found_words])/len(found_words)))

print(*found_words, sep=' ')

