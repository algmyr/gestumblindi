#!/usr/bin/env python

import gestum
import gestum_sort
import time

class Timer:    
    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.interval = self.end - self.start

def run_test(module, phrase, dictionaries):
    print("\n========== Running %s  ==========\n" % (module.__name__))

    T_index = []
    T_search = []
    W_found = []

    for dictionary in dictionaries:
        print("\nDictionary: %s\n" % (dictionary))
        with Timer() as t:
            index,words = module.index_file(dictionary)
        print('Read+indexing: %.05f sec.' % t.interval)
        T_index.append(t.interval)

        with Timer() as t:
            found_words = module.find_words(phrase, index, words)
        print('Word search:   %.05f sec.' % t.interval)
        T_search.append(t.interval)
        
        W_found.append(len(found_words))

    return T_index, T_search, W_found

def write_data(path, dictionaries, N, D_g, D_gs):
    with open(path, 'w') as f:
        f.write("% ,,    gestum.py,,,                 gestum_sort.py,,\n")
        f.write("% Dict, N, Indexing time (s), Search time (s), Word count, Indexing time (s), Search time (s), Word count\n")
        for i in range(len(dictionaries)):
            #data = [dictionaries[i]]
            data = [N[i]]
            data.extend([x[i] for x in D_g+D_gs])

            fmt = "{0}, {1}, {2}, {3}, {4}, {5}, {6}\n"

            f.write(fmt.format(*data))


phrase = "optimizationmatters"

dictionaries_by_size = [
    "Dicts/SizeLimited/100dict",
    "Dicts/SizeLimited/1000dict",
    "Dicts/SizeLimited/10000dict",
    "Dicts/SizeLimited/100000dict",
    "Dicts/SizeLimited/200000dict",
    "Dicts/SizeLimited/300000dict",
    "Dicts/SizeLimited/400000dict"
]

D_gestum_size = run_test(gestum, phrase, dictionaries_by_size)
D_gestum_sort_size = run_test(gestum_sort, phrase, dictionaries_by_size)

N = (100, 1000, 10000, 100000, 200000, 300000, 400000)
write_data('size_stats.csv', dictionaries_by_size, N, D_gestum_size, D_gestum_sort_size)

###

dictionaries_by_charcount = [
    "Dicts/CharLimited/3words",
    "Dicts/CharLimited/5words",
    "Dicts/CharLimited/7words",
    "Dicts/CharLimited/9words",
    "Dicts/CharLimited/11words",
    "Dicts/CharLimited/13words",
    "Dicts/CharLimited/15words",
    "Dicts/CharLimited/17words"
]

D_gestum_char = run_test(gestum, phrase, dictionaries_by_charcount)
D_gestum_sort_char = run_test(gestum_sort, phrase, dictionaries_by_charcount)

N = (3,5,7,9,11,13,15,17)
write_data('charcount_stats.csv', dictionaries_by_charcount, N, D_gestum_char, D_gestum_sort_char)

