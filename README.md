Gestumblindi
============

My solution to a challenge posed by Jeppesen.

The challenge was, in short:
    You're given a phrase (a collection of letters).
    Find all english words in a list that can be formed from letters in that phrase.

gestum.py
---------
The solution is quite quick and can easily handle huge wordlists (reading and
indexing a 1.6 million word list takes 6-7 seconds) and long phrases (~50 random
chars takes 2-3 seconds). All in all I think this a quite capable solution,
although improvements can always be made.

Smart indexing of the word list makes the search a lot faster. What is by nature
a O(n!) problem can be turned into seemingly polynolmial problem on average.

See gestum.py for the code with lengthy explanations of roughly how things work,
complete with some time and space complexities. The nature of the problem
(combinatorics) gives me huge worse cases, but very good average cases (read
O(n!) compared to O(n^p)). So don't be discouraged if my worst cases looks
terrible.

gestum-sort.py
--------------
A variant of the above solution where I apply the realization that letter
ordering does not matter. This variand indexes the sorted version of words,
which costs extra time (a factor n log(n)). In return the search time lowers
dramatically due to a smaller tree and checking only for lexiographically sorted
arrangements.

Comparison using the 1.6M dictionary:
    gestum:

        =============== Running gestum ===============
        Indexing:    5.98689 sec.
        Word search: 0.392 sec.

        ========== Running gestum_sort ==========
        Indexing:    8.33278 sec.
        Word search: 0.086 sec.

General stuff
-------------
Your choice is to either spend more time pre-computing the index (which could be
stored and re-used) or spend more time searching.

More thorough comparisons can be gotten with the `test.py` file which generates csv
files for different dictionaries. The plotted results can be generated with the
MATLAB/Octave file `plotdata.m`, or check the Plots directory for plots of the
csv files included.

Notes:
To run the scripts you'll have to uncompress the dictionaries in the Dicts
directory first!
The scripts are only tested from a terminal, so running a script from a GUI
might behave oddly (no pausing after script done and such).

// Anton Älgmyr, Teknisk Fysik @ Chalmers University of Technology