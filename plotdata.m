S = load('size_stats.csv');

N =    S(:,1);
T_I1 = S(:,2)*1000;
T_S1 = S(:,3)*1000;
T_W1 = S(:,4);
T_I2 = S(:,5)*1000;
T_S2 = S(:,6)*1000;
T_W2 = S(:,7);

figure;
clf;
hold on;
    plot(N, T_I1, 'b-');
    plot(N, T_I2, 'r-');
    title('Size stats, index time [blue is gestum, red is gestum_sort]');
    xlabel('Size of wordlist');
    ylabel('Time [ms]');
hold off;

figure;
clf;
hold on;
    plot(N, T_S1, 'b-');
    plot(N, T_S2, 'r-');
    title('Size stats, search time [blue is gestum, red is gestum_sort]');
    xlabel('Size of wordlist');
    ylabel('Time [ms]');
hold off;

figure;
clf;
hold on;
    plot(N, T_W1, 'bx');
    plot(N, T_W2, 'r+');
    title('Size stats, word count [blue is gestum, red is gestum_sort]');
    xlabel('Size of wordlist');
    ylabel('Words found');
hold off;

%%

C = load('charcount_stats.csv');

N =    C(:,1);
T_I1 = C(:,2)*1000;
T_S1 = C(:,3)*1000;
T_W1 = C(:,4);
T_I2 = C(:,5)*1000;
T_S2 = C(:,6)*1000;
T_W2 = C(:,7);

figure;
clf;
hold on;
    plot(N, T_I1, 'b-');
    plot(N, T_I2, 'r-');
    title('Char count stats, index time [blue is gestum, red is gestum_sort]');
    xlabel('Word length in list');
    ylabel('Time [ms]');
hold off;

figure;
clf;
hold on;
    plot(N, T_S1, 'b-');
    plot(N, T_S2, 'r-');
    title('Char count stats, search time [blue is gestum, red is gestum_sort]');
    xlabel('Word length in list');
    ylabel('Time [ms]');
hold off;

figure;
clf;
hold on;
    plot(N, T_W1, 'bx');
    plot(N, T_W2, 'r+');
    title('Char count stats, word count [blue is gestum, red is gestum_sort]');
    xlabel('Word length in list');
    ylabel('Words found');
hold off;
