#!/usr/bin/env python

import gestum
import gestum_sort
import time
import argparse

class Timer:    
    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.interval = self.end - self.start

def run(module, phrase, dictionary, N, WORDS=True, TIME=False, STATS=False):
    print("\n========== Running %s ==========\n" % (module.__name__))

    with Timer() as t:
        index,words = module.index_file(dictionary)
    if TIME: print('Read+indexing: %.05f sec.' % t.interval)

    with Timer() as t:
        found_words = module.find_words(phrase, index, words, N)
    if TIME: print('Word search:   %.05f sec.' % t.interval)
    
    if WORDS: print(*sorted(found_words), sep='\n')

    # Print some stats
    if STATS:
        if len(words) != 0:
            print('\nWords in word list: %d words' % (len(words)))
            print('Average word length in word list: %.01f characters.' % (sum([len(w) for w in words])/len(words)))
        else:
            print("\nDictionary is empty!")
        if len(found_words) != 0:
            print('Number of words in \"%s\": %d.' % (phrase, len(found_words)))
            print('Average word length among found words: %.01f characters.\n' % (sum([len(w) for w in found_words])/len(found_words)))
        else:
            print("No words were found!\n")


# Parse arguments
parser = argparse.ArgumentParser(description='Solution to the Gestumblindi challenge.')
parser.add_argument('-d', '--dictionary', default='./Dicts/big-dict',
        help="Path to dictionary path", required=False)
parser.add_argument('-p', '--phrase', default='optimizationmatters',
        help="Phrase to search in", required=False)
parser.add_argument('-n', '--charcount', default=3,
        help="The number to limit the search to in the first search", required=False)
parser.add_argument('--stats', action='store_true',
        help="Ignore words and print statistics", required=False)
args = parser.parse_args()

phrase = args.phrase
N = int(args.charcount)
dictionary = args.dictionary
STATS = args.stats

# Flag setups
STATSNOWORDS = (False, True, True)
ONLYWORDS = (True, False, False)


# Actually run stuff

if STATS:
    print('\n------ Limited to %d chars ------' % N)
    run(gestum, phrase, dictionary, N, *STATSNOWORDS)
    run(gestum_sort, phrase, dictionary, N, *STATSNOWORDS)
    
    print('\n------ Any number of chars ------')
    run(gestum, phrase, dictionary, None, *STATSNOWORDS)
    run(gestum_sort, phrase, dictionary, None, *STATSNOWORDS)
else:
    print('\n------ Limited to %d chars ------' % N)
    run(gestum, phrase, dictionary, None, *ONLYWORDS)
    run(gestum_sort, phrase, dictionary, None, *ONLYWORDS)
    
    print('\n------ Any number of chars ------')
    run(gestum, phrase, dictionary, N, *ONLYWORDS)
    run(gestum_sort, phrase, dictionary, N, *ONLYWORDS)

